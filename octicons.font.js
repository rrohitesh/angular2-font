module.exports = {
    fontName: "Octicons",
    files: ["./octicons/svg/*.svg"],
    baseClass: "octicon",
    classPrefix: "octicon-"
};
